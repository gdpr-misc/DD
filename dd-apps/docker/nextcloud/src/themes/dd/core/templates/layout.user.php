<?php
/*
#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
*/
// \OCP\Util::addScript('dd', 'navbar');
// style('dd', 'ddstyles');
$api_url = "https://" . preg_replace('/^nextcloud\./', 'api.', $_SERVER['HTTP_HOST']);
$profile_url = "https://" . preg_replace('/^nextcloud\./', 'sso.', $_SERVER['HTTP_HOST']) . "/auth/realms/master/account";
$showsettings = in_array($_['user_uid'], ['admin', 'ddadmin']) ? true : false;
 
 $getUserAvatar = static function (int $size) use ($_): string {
	return \OC::$server->getURLGenerator()->linkToRoute('core.avatar.getAvatar', [
		'userId' => $_['user_uid'],
		'size' => $size,
		'v' => $_['userAvatarVersion']
	]);
}
//script('dd', 'navbar.js');
?><!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="<?php p($_['language']); ?>" data-locale="<?php p($_['locale']); ?>" >
	<head data-user="<?php p($_['user_uid']); ?>" data-user-displayname="<?php p($_['user_displayname']); ?>" data-requesttoken="<?php p($_['requesttoken']); ?>">
		<meta charset="utf-8">
		<title>
			<?php
				p(!empty($_['pageTitle'])?$_['pageTitle'].' - ':'');
				p(!empty($_['application'])?$_['application'].' - ':'');
				p($theme->getTitle());
			?>
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<?php if ($theme->getiTunesAppId() !== '') { ?>
		<meta name="apple-itunes-app" content="app-id=<?php p($theme->getiTunesAppId()); ?>">
		<?php } ?>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-title" content="<?php p((!empty($_['application']) && $_['appid'] != 'files')? $_['application']:$theme->getTitle()); ?>">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="theme-color" content="<?php p($theme->getColorPrimary()); ?>">
		<link rel="icon" href="<?php print_unescaped(image_path($_['appid'], 'favicon.ico')); /* IE11+ supports png */ ?>">
		<link rel="apple-touch-icon" href="<?php print_unescaped(image_path($_['appid'], 'favicon-touch.png')); ?>">
		<link rel="apple-touch-icon-precomposed" href="<?php print_unescaped(image_path($_['appid'], 'favicon-touch.png')); ?>">
		<link rel="mask-icon" sizes="any" href="<?php print_unescaped(image_path($_['appid'], 'favicon-mask.svg')); ?>" color="<?php p($theme->getColorPrimary()); ?>">
		<link rel="manifest" href="<?php print_unescaped(image_path($_['appid'], 'manifest.json')); ?>">
		<link type="text/css"  rel="stylesheet" href="<?php p($api_url) ?>/css/font-awesome-4.7.0/css/font-awesome.min.css">
		<link type="text/css"  rel="stylesheet" href="<?php p($api_url) ?>/css/dd.css">
		<link type="text/css" rel="stylesheet" href="/themes/dd/core/css/ddstyles.css">
		<script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>" src="/themes/dd/core/js/navbar.js"></script>
		<?php emit_css_loading_tags($_); ?>
		<?php emit_script_loading_tags($_); ?>
		<?php print_unescaped($_['headers']); ?>
	</head>
	<?php array_push($_['enabledThemes'], "light");?>
	<body id="<?php p($_['bodyid']);?>" <?php foreach ($_['enabledThemes'] as $themeId) {
				p("data-theme-$themeId ");
			}?> data-themes=<?php p(join(',', $_['enabledThemes'])) ?>>
	<?php include_once (__DIR__ . '/../../../../core/templates/layout.noscript.warning.php'); ?>

		<?php foreach ($_['initialStates'] as $app => $initialState) { ?>
			<input type="hidden" id="initial-state-<?php p($app); ?>" value="<?php p(base64_encode($initialState)); ?>">
		<?php }?>

		<div id="skip-actions">
			<?php if ($_['id-app-content'] !== null) { ?><a href="<?php p($_['id-app-content']); ?>" class="button primary skip-navigation skip-content"><?php p($l->t('Skip to main content')); ?></a><?php } ?>
			<?php if ($_['id-app-navigation'] !== null) { ?><a href="<?php p($_['id-app-navigation']); ?>" class="button primary skip-navigation"><?php p($l->t('Skip to navigation of app')); ?></a><?php } ?>
		</div>

		<header role="banner" id="header">
			<h1 class="hidden-visually" id="page-heading-level-1">
				<?php p(!empty($_['pageTitle'])?$_['pageTitle']:$theme->getName()); ?>
			</h1>
			<div class="header-left" id="navbar-logo">
				<a href="<?php print_unescaped($_['logoUrl'] ?: link_to('', 'index.php')); ?>"
					id="nextcloud">
					<img src="<?php p($api_url) ?>/img/logo.png" alt="" style="height: 39px;">
				</a>

				<!--<nav id="header-left__appmenu"></nav>-->
			</div>

			<div class="header-right" id="navbar-nextcloud">
				<div id="unified-search"></div>
				<div id="notifications"></div>
				<div id="contactsmenu"></div>
				<div id="dd-megamenu"></div>
<?php if ($showsettings) { // Native NextCloud Menu ?>
				<div id="user-menu"></div>
<?php } else { // Filtered menu ?>
				<div id="dd-user-menu"><div id='dd-user-menu-button'>
					<div id="dd-user-menu-trigger" data-toggle='dropdown'>
						<div id="avatardiv-menu" class="avatardiv<?php if ($_['userAvatarSet']) {
				print_unescaped(' avatardiv-shown');
			} else {
				print_unescaped('" style="display: none');
			} ?>"
							 data-user="<?php p($_['user_uid']); ?>"
							 data-displayname="<?php p($_['user_displayname']); ?>"
			<?php
			if ($_['userAvatarSet']) {
				$avatar39 = $getUserAvatar(39); ?> data-avatar="<?php p($avatar39); ?>"
			<?php
			} ?>>
							<?php
							if ($_['userAvatarSet']) {?>
								<img alt="" width="39" height="39"
								src="<?php p($avatar39);?>"
								srcset="<?php p($getUserAvatar(64));?> 2x, <?php p($getUserAvatar(128));?> 4x"
								>
							<?php } ?>
						</div>
					</div>
					<div id="dd-user-menu-dropdown" class='dropdown-menu dropdown-menu-right' labeledby='dd-user-menu-trigger' style='display:none'>
						<ul>
							<li>
								<a target="_blank" href="<?php p($profile_url)?>">
									<div class="ddiconcontainer">
										<i class="icon fa fa-user fa-fw ddicon"></i>
									</div>
									<?php p($_['user_displayname']); ?>
								</a>
							</li>
							<?php $entry = json_decode($_['initialStates']['core-settingsNavEntries'], true)['logout']; ?>
							<li data-id="<?php p($entry['id']); ?>">
								<a href="<?php print_unescaped($entry['href']); ?>">
									<div class="ddiconcontainer">
										<i class="icon fa fa-sign-out fa-fw ddicon" aria-hidden="true"></i> </div>
										<?php p($entry['name']) ?>
								</a>
							</li>
						</ul>
					</div>
				</div></div>
			<?php } ?>
				<div id="product-logo">
					<a href="https://xnet-x.net/ca/digital-democratic/" target="_blank">
						<img src="/themes/dd/core/img/dd.svg" alt="" style="height: 16px; margin-right: 21px; margin-top: 16px"/>
					</a>
				</div>
			</div>
		</header>

		<div id="sudo-login-background" class="hidden"></div>
		<form id="sudo-login-form" class="hidden" method="POST">
			<label>
				<?php p($l->t('This action requires you to confirm your password')); ?><br/>
				<input type="password" class="question" autocomplete="new-password" name="question" value=" <?php /* Hack against browsers ignoring autocomplete="off" */ ?>"
				placeholder="<?php p($l->t('Confirm your password')); ?>" />
			</label>
			<input class="confirm" value="<?php p($l->t('Confirm')); ?>" type="submit">
		</form>

		<main id="content" class="app-<?php p($_['appid']) ?>">
			<?php print_unescaped($_['content']); ?>
		</main>
		<div id="profiler-toolbar"></div>
	</body>

</html>
