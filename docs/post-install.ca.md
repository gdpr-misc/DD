# Instruccions post-instal·lació

Tot i que l'[instal·lador](install.ca.md) automatitza la majoria de les
configuracions, encara és necessari fer alguns passos de forma manual.

## Accessos sense SAML

Un cop instal·lat es pot accedir als serveis sense SAML, per exemple
per finalitzar o comprovar alguna configuració.

Les dades d'inici de sessió es troben a `dd.conf`.

| Servei | Variables | Login sense SAML |
| ------ | --------- | ---------------- |
| Moodle | `MOODLE_ADMIN_*` |`https://moodle.DOMINI/login/index.php?saml=off` |
| Nextcloud | `NEXTCLOUD_ADMIN_*` | `https://nextcloud.DOMINI/login?direct=1` |
| Wordpress | `WORDPRESS_ADMIN_*` | `https://wp.DOMINI/wp-login.php?normal` |
| Keycloak | `KEYCLOAK_*` | `https://sso.DOMINI/auth/admin/master/console` |
| Admin | `DDADMIN_*` | `https://admin.DOMINI` |


## Usuari SAML per fer proves

Per poder comprovar tots els serveis cal crear el primer usuari SAML.
Això es fa en l'aplicació admin.

- anar a https://admin.DOMAIN
- crear un grup, per exemple: "docents"
- clic al botó d'administration Resync
- anar a groups i verificar que apareix
- anar a users i crear l'usuari "docent01" del grup "docents" amb role "teacher"

## Activar WAF

Si així ho volem, podem activar el Web Application Firewall/Modsecurity seguint
[aquestes instruccions](waf-modsecurity.es.md).

## Plantilles Nextcloud (Opcional)

Opcionalment, si voleu configurar les plantilles per tots els usuaris:

![](img/snapshot/Y!-rq;7GxjTW.png)


## Integració Moodle-Nextcloud

La integració Moodle-Nextcloud no està automatitzada i cal seguir aquests
passos en finalitzar la instal·lació de DD.


### Crear client OAuth en Nextcloud

![](img/snapshot/3ICWP5X.png)

- Name: moodle
- URI: https://moodle.test1.digitaldemocratic.net/admin/oauth2callback.php

Es crea un **Id Client** i un **Secret** que cal afegir en el oAuth2 de Moodle.


### Crear servei OAuth2 a Moodle

https://moodle.test1.digitaldemocratic.net/admin/tool/oauth2/issuers.php

Crear nou servei Nextcloud

![](img/snapshot/mkM8JN1.png)

Configurar amb aquestes dades:

- Name: Nextcloud
- Client Id: **Id Client**
- Client Secret: **Secret**
- [OK] Autenticar solicituts de token a través de encapçalats HTTP
- URL base de servici: https://nextcloud.test1.digitaldemocratic.net

![](img/snapshot/KBV5ys2.png)

Per provar que funciona donem a la següent icona
![](img/snapshot/XLQNA9i.png)

I seguim els passos d´autenticació que ens marca Nextcloud. Si apareix el Tic verd, estaria ben configurat.

### Activar repositori en Moodle
3. Cal anar a 'Manage repositories' https://moodle.test1.digitaldemocratic.net/admin/repository.php

Activar i posar visible

Anar a Settings del Repositori Nextcloud

![](img/snapshot/JGRbAJF.png)

Activar les dues opcions i salvar

![](img/snapshot/buRSMwg.png)

Crear una instància del Repositori

- Name: Nextcloud
- Issuer: Seleccionem l'oAuth2 que hem creat anteriorment
- Folder: ''
- Supported files: Internal y External
- Return typ: Internal

`[DESAR CANVIS]`
