{
    "info": {
        "title": "DD API NotaBLE",
        "description": "NotaBLE \u00e9s la col\u00b7laboraci\u00f3 entre Gwido i el Workspace educatiu DD.\n\n\u00c9s un projecte de Xnet, IsardVDI, Gwido i Taller de M\u00fasics, guanyador de la Ciutat Proactiva 2021, suport a la innovaci\u00f3 urbana de la Fundaci\u00f3 BitHabitat.",
        "version": "2022.11.0",
        "termsOfService": ""
    },
    "paths": {
        "/ddapi/users": {
            "get": {
                "summary": "List all registered users on DD.",
                "responses": {
                    "200": {
                        "description": "The list of users registered on DD",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        },
                        "examples": "[{\n  \"keycloak_id\": \"a773d249-a113-4542-8101-3a50f4cd28c2\",\n  \"id\": \"ID\",\n  \"username\": \"ID\",\n  \"enabled\": true,\n  \"first\": \"NAME\",\n  \"last\": \"LASTNAME\",\n  \"role\": \"student\",\n  \"email\": \"ID@DOMAIN\",\n  \"groups\": [\n    \"GRUP\",\n    \"student\"\n  ],\n  \"quota\": \"500 MB\",\n  \"quota_used_bytes\": \"0 MB\"\n}]\n"
                    }
                }
            }
        },
        "/ddapi/users/filter": {
            "post": {
                "summary": "List registered users on DD with a filter applied.",
                "responses": {
                    "200": {
                        "description": "The list of users registered on DD with the filter applied.\n",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                },
                "parameters": [
                    {
                        "in": "body",
                        "name": "filter",
                        "description": "The filter to apply to users registered on DD",
                        "schema": {
                            "type": "object",
                            "required": [
                                "filter"
                            ],
                            "properties": {
                                "text": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                ],
                "consumes": [
                    "application/json"
                ]
            }
        },
        "/ddapi/groups": {
            "get": {
                "summary": "List all registered groups on DD.",
                "responses": {
                    "200": {
                        "description": "The list of groups registered on DD",
                        "schema": {
                            "$ref": "#/definitions/Groups"
                        },
                        "examples": "[{\n  \"keycloak_id\": \"f6ec2bda-bec9-415f-bcb7-f5ae644bfec5\",\n  \"id\": \"ID\",\n  \"name\": \"NAME\",\n  \"path\": \"PATH\",\n  \"description\": \"DESCRIPITON\",\n}]\n"
                    }
                }
            }
        },
        "/ddapi/roles": {
            "get": {
                "summary": "List all roles configured on DD.",
                "responses": {
                    "200": {
                        "description": "The list of roles configured on DD",
                        "schema": {
                            "$ref": "#/definitions/Roles"
                        },
                        "examples": "[{\n  \"keycloak_id\": \"f6ec2bda-bec9-415f-bcb7-f5ae644bfec5\",\n  \"id\": \"ID\",\n  \"name\": \"NAME\",\n  \"description\": \"DESCRIPITON\",\n}]\n"
                    }
                }
            }
        },
        "/ddapi/role/users": {
            "post": {
                "summary": "List registered users on DD with a the given role.",
                "responses": {
                    "200": {
                        "description": "The list of users registered on DD with the filter applied.\n",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                },
                "parameters": [
                    {
                        "in": "body",
                        "name": "role",
                        "description": "The role to search users registered on DD.\nOne of 'id', 'name' and 'keycloak_id' must be provided.\nThis is also the order in which the parameters are checked,\nin case multiple are provided.\n",
                        "schema": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "string",
                                    "required": false
                                },
                                "name": {
                                    "type": "string",
                                    "required": false
                                },
                                "keycloak_id": {
                                    "type": "string",
                                    "format": "uuid",
                                    "required": false
                                }
                            }
                        }
                    }
                ],
                "consumes": [
                    "application/json"
                ]
            }
        },
        "/ddapi/user/{user_ddid}": {
            "put": {
                "summary": "Modify a user in DD",
                "responses": {
                    "200": {
                        "schema": {
                            "type": "object"
                        }
                    },
                    "404": {
                        "description": "The user does not exist\n"
                    }
                },
                "parameters": [
                    {
                        "in": "body",
                        "name": "user",
                        "description": "The user to be modified on DD.\n",
                        "schema": {
                            "type": "object",
                            "properties": {
                                "first": {
                                    "required": false,
                                    "type": "string"
                                },
                                "last": {
                                    "required": false,
                                    "type": "string"
                                },
                                "email": {
                                    "required": false,
                                    "type": "string",
                                    "format": "email"
                                },
                                "password": {
                                    "required": false,
                                    "type": "string",
                                    "format": "email"
                                },
                                "password_temporary": {
                                    "required": false,
                                    "type": "bool"
                                },
                                "quota": {
                                    "required": false,
                                    "type": "string"
                                },
                                "enabled": {
                                    "required": false,
                                    "type": "bool"
                                },
                                "role": {
                                    "required": false
                                },
                                "groups": {
                                    "required": false,
                                    "type": "array",
                                    "items": {
                                        "type": "string"
                                    }
                                }
                            }
                        }
                    }
                ],
                "consumes": [
                    "application/json"
                ]
            },
            "delete": {
                "summary": "Delete a registered user in DD",
                "responses": {
                    "200": {
                        "schema": {
                            "type": "object"
                        }
                    },
                    "404": {
                        "description": "The user does not exist\n"
                    }
                },
                "parameters": [
                    {
                        "in": "url",
                        "name": "user_ddid",
                        "description": "The user to delete from DD\n",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "consumes": [
                    "application/json"
                ]
            },
            "get": {
                "summary": "Get a registered user in DD",
                "responses": {
                    "200": {
                        "description": "The user as it exists on DD\n",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    },
                    "404": {
                        "description": "The user does not exist\n"
                    }
                },
                "parameters": [
                    {
                        "in": "url",
                        "name": "user_ddid",
                        "description": "The user to retrieve from DD\n",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "consumes": [
                    "application/json"
                ]
            }
        },
        "/ddapi/user": {
            "post": {
                "summary": "Register a new user in DD",
                "responses": {
                    "200": {
                        "description": "The keycloak_id of the newly registered user\n",
                        "schema": {
                            "$ref": "#/definitions/KeycloakId"
                        },
                        "examples": "{ \"keycloak_id\": \"f6ec2bda-bec9-415f-bcb7-f5ae644bfec5\" }\n"
                    },
                    "409": {
                        "description": "The user already exists\n"
                    }
                },
                "parameters": [
                    {
                        "in": "body",
                        "name": "user",
                        "description": "The user to be registered on DD.\n",
                        "schema": {
                            "type": "object",
                            "properties": {
                                "username": {
                                    "required": true,
                                    "type": "string"
                                },
                                "first": {
                                    "required": true,
                                    "type": "string"
                                },
                                "last": {
                                    "required": true,
                                    "type": "string"
                                },
                                "email": {
                                    "required": true,
                                    "type": "string",
                                    "format": "email"
                                },
                                "password": {
                                    "required": true,
                                    "type": "string",
                                    "format": "email"
                                },
                                "password_temporary": {
                                    "required": false,
                                    "type": "bool"
                                },
                                "quota": {
                                    "required": true,
                                    "type": "string"
                                },
                                "enabled": {
                                    "required": true,
                                    "type": "bool"
                                },
                                "role": {
                                    "required": true
                                },
                                "groups": {
                                    "required": true,
                                    "type": "array",
                                    "items": {
                                        "type": "string"
                                    }
                                }
                            }
                        }
                    }
                ],
                "consumes": [
                    "application/json"
                ]
            }
        },
        "/ddapi/group/{group_id}": {
            "delete": {
                "summary": "Delete a registered group in DD",
                "responses": {
                    "200": {
                        "schema": {
                            "type": "object"
                        }
                    },
                    "404": {
                        "description": "The group does not exist\n"
                    }
                },
                "parameters": [
                    {
                        "in": "url",
                        "name": "group_id",
                        "description": "The group to delete from DD\n",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "consumes": [
                    "application/json"
                ]
            },
            "get": {
                "summary": "Get a registered group in DD",
                "responses": {
                    "200": {
                        "description": "The group as it exists on DD\n",
                        "schema": {
                            "$ref": "#/definitions/Group"
                        }
                    },
                    "404": {
                        "description": "The group does not exist\n"
                    }
                },
                "parameters": [
                    {
                        "in": "url",
                        "name": "group_id",
                        "description": "The group to retrieve from DD\n",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "consumes": [
                    "application/json"
                ]
            }
        },
        "/ddapi/group": {
            "post": {
                "summary": "Register a new group in DD",
                "responses": {
                    "200": {
                        "description": "The keycloak_id of the newly registered group\n",
                        "schema": {
                            "$ref": "#/definitions/KeycloakId"
                        }
                    },
                    "409": {
                        "description": "The group already exists\n"
                    }
                },
                "parameters": [
                    {
                        "in": "body",
                        "name": "group",
                        "description": "The group to be registered on DD.\n",
                        "schema": {
                            "type": "object",
                            "properties": {
                                "name": {
                                    "required": true,
                                    "type": "string"
                                },
                                "description": {
                                    "required": false,
                                    "type": "string"
                                },
                                "parent": {
                                    "required": false,
                                    "type": "string"
                                }
                            }
                        }
                    }
                ],
                "consumes": [
                    "application/json"
                ]
            }
        }
    },
    "definitions": {
        "User": {
            "type": "object",
            "properties": {
                "keycloak_id": {
                    "type": "string",
                    "format": "uuid"
                },
                "id": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                },
                "enabled": {
                    "type": "boolean"
                },
                "first": {
                    "type": "string"
                },
                "last": {
                    "type": "string"
                },
                "role": {
                    "type": "string"
                },
                "email": {
                    "type": "string",
                    "fomat": "email"
                },
                "groups": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "quota": {
                    "type": "string"
                },
                "quota_used_bytes": {
                    "type": "string"
                }
            }
        },
        "Users": {
            "type": "array",
            "items": {
                "$ref": "#/definitions/User"
            }
        },
        "Group": {
            "type": "object",
            "properties": {
                "keycloak_id": {
                    "type": "string",
                    "format": "uuid"
                },
                "id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "path": {
                    "type": "string"
                },
                "description": {
                    "type": "string"
                }
            }
        },
        "Groups": {
            "type": "array",
            "items": {
                "$ref": "#/definitions/Group"
            }
        },
        "Role": {
            "type": "object",
            "properties": {
                "keycloak_id": {
                    "type": "string",
                    "format": "uuid"
                },
                "id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "description": {
                    "type": "string"
                }
            }
        },
        "Roles": {
            "type": "array",
            "items": {
                "$ref": "#/definitions/Role"
            }
        },
        "KeycloakId": {
            "type": "object",
            "properties": {
                "keycloak_id": {
                    "required": true,
                    "type": "string"
                }
            }
        }
    },
    "swagger": "2.0",
    "externalDocs": {
        "description": "Online Documentation",
        "url": "https://dd.digitalitzacio-democratica.xnet-x.net/docs/integrations.ca/"
    },
    "securityDefinitions": {
        "dd_jwt": {
            "type": "apiKey",
            "in": "header",
            "name": "Authorization",
            "description": "JWS token using API_SECRET (e.g. 'bearer X.Y')"
        }
    },
    "security": {
        "dd_jwt": {
            "$ref": "#/securityDefinitions/dd_jwt"
        }
    },
    "swagger_ui": true
}
