# Seguretat

## Configuracions de DD

Actualment el DD te les següents opcions específicament relacionades amb
seguretat:

### Web Application Firewall (WAF) / Modsecurity

Podem activar el Web Application Firewall/Modsecurity seguint
[aquestes instruccions](waf-modsecurity.es.md).

### ClamAV / Antivirus

De manera similar al [WAF](waf-modsecurity.es.md), podem activar `ClamAV`
posant la variable `DISABLE_CLAMAV=true` al `dd.conf` i executant:

```sh
# Regenerar el docker-compose.yml
./dd-ctl yml
# Engegar el contenidor
./dd-ctl up
# Aplicar configuracions específiques de ClamAV als altres serveis
./dd-ctl personalize
```

## Seguretat del sistema en general

La seguretat d'un sistema pot ser complexa, aquí intentem fixar criteris
generals que puguin ajudar a assegurar el vostre sistema.

Tingueu en compte però que, com sempre, haureu d'aplicar el vostre criteri
professional i respectar els vostres requeriments i necessitats.

### El fitxer `dd.conf`

Aquesta és la configuració principal del sistema, **únicament els operadors del
sistema** hi han de tenir accés!

Reviseu amb noves versions de DD els canvis a `dd.conf.sample` i adjusteu el
vostre `dd.conf` d'acord a aquests canvis.

### Tallafocs / firewall

Com amb qualsevol servei exposat a internet, és molt important tenir un
tallafocs ben configurat, el DD només necessita exposar a internet els
ports TCP 80/HTTPS i 443/HTTPS.

Una opció pot ser fer servir `ufw` amb regles per defecte de `deny`, excepte
per els ports abans esmentats.
Aneu amb compte per no quedar-vos fora del sistema!
Per això vegeu tot seguit [Accés SSH](#acces-ssh).

### Accés SSH

Idealment configureu el tallafocs per no permetre des de internet l'accés al
port TCP 22/SSH.

Si no compteu amb una VPN, però sí amb un servidor bastió / una IP pública, una
opció és que només permeteu l'accés al port TCP 22/SSH des d'aquesta o aquestes
IPs públiques.

Si teniu una VPN, aquesta opció és millor i caldrà configurar a
`/etc/ssh/sshd_config` la opció `ListenAddress` per tal que només estigui permès
a nivell de software la connexió des del rang de IPs de la vostra VPN.

### Autenticació SSH

En **cap cas** hauríeu de tenir **autenticació SSH** amb **contrasenya**.

Feu servir **sempre claus asimètriques** i, si potser, un dispositiu físic que
guardi la vostra clau privada de forma segura, com ara una
[YubiKey](https://yubico.com).

### Detecció d'intrusions

Es recomana desplegar `rkhunter` per detectar anomalies en el sistema.
Podeu consultar recomanacions de configuració a aquesta [wiki][serverstats].

## Altres recursos

També podeu consultar [aquesta documentació pública][serverstats] sobre
qüestions de seguretat i nombrosos altres recursos a internet.

[serverstats]: https://gitlab.com/MaadiX/server-stats-and-check/-/wikis/en_home
