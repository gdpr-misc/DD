#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import json
import logging as log
import os
import sys
import time
import traceback
from typing import TYPE_CHECKING, Union
from uuid import uuid4

from flask import Response, jsonify, redirect, render_template, request, url_for

if TYPE_CHECKING:
    from api.flaskapp import ApiFlaskApp

from ..lib.menu import Menu
from .decorators import JsonResponse


def setup_menu_views(app: "ApiFlaskApp") -> None:
    menu = Menu(app)

    @app.route("/header/<format>", methods=["GET"])
    @app.route("/header/<format>/<application>", methods=["GET"])
    def api_v2_header(
        format: str, application: Union[str, bool] = False
    ) -> Union[str, JsonResponse]:
        if format == "json":
            return (
                json.dumps(menu.get_header()),
                200,
                {"Content-Type": "application/json"},
            )
        else:  # format == "html":
            if application == "nextcloud":
                return render_template("header_nextcloud.html")
            if application == "wordpress":
                return render_template("header_wordpress.html")
            else:  # if application == False:
                return render_template("header.html")

    # @app.route('/user_menu/<format>', methods=['GET'])
    # @app.route('/user_menu/<format>/<application>', methods=['GET'])
    # def api_v2_user_menu(format,application=False):
    #     if application == False:
    #         if format == 'json':
    #             if application == False:
    #                 return json.dumps(menu.get_user_nenu()), 200, {'Content-Type': 'application/json'}
