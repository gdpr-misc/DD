#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from eventlet import monkey_patch

monkey_patch()

from flask_socketio import SocketIO

from api import get_app

# Setup the app
app = get_app()
app.setup()

app.socketio = SocketIO(app)

if __name__ == "__main__":
    import logging

    logger = logging.getLogger("socketio")
    logger.setLevel("ERROR")
    engineio_logger = logging.getLogger("engineio")
    engineio_logger.setLevel("ERROR")

    print("Starting dd-sso api...")
    app.socketio.run(app, host="0.0.0.0", port=7039, debug=False)
