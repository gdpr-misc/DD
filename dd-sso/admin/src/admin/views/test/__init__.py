"""
Tests for admin.views, particularly the NotaBLE API.

NotaBLE és la col·laboració entre Gwido i el Workspace educatiu DD.

És un projecte de Xnet, IsardVDI, Gwido i Taller de Músics,
guanyador de la Ciutat Proactiva 2021, suport a la innovació urbana de
la Fundació BitHabitat.
"""
