//
//   Copyright © 2021,2022 IsardVDI S.L.
//
//   This file is part of DD
//
//   DD is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or (at your
//   option) any later version.
//
//   DD is distributed in the hope that it will be useful, but WITHOUT ANY
//   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//   details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with DD. If not, see <https://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL-3.0-or-later

$(document).on('shown.bs.modal', '#modalAddDesktop', function () {
    modal_add_desktops.columns.adjust().draw();
}); 

$(document).ready(function() {

    $('.btn-global-resync').on('click', function () {
        $.ajax({
            type: "GET",
            url:"api/resync",
            success: function(data)
            {
                table.ajax.reload();
                // $("#modalImport").modal('hide');
                // users_table.ajax.reload();
                // groups_table.ajax.reload();
            },
            error: function(data)
            {
                alert('Something went wrong on our side...')
            }
        });
    });
    
	// Open new group modal
	$('.btn-new').on('click', function () {
        $('#modalAddGroup').modal({
            backdrop: 'static',
            keyboard: false
        }).modal('show');
    });

    // Send new group form
    $('#modalAddGroup #send').on('click', function () {
        var form = $('#modalAddGroupForm');
        formdata = form.serializeObject()
        console.log('NEW GROUP')
        console.log(formdata)
        // $.ajax({
        //     type: "POST",
        //     "url": "/groups_list",
        //     success: function(data)
        //     {
        //         console.log('SUCCESS')
        //         // $("#modalAddGroup").modal('hide');
        //     },
        //     error: function(data)
        //     {
        //         alert('Something went wrong on our side...')
        //     }
        // });
    });

    $('.btn-delete_keycloak').on('click', function () {
        $.ajax({
            type: "DELETE",
            url:"/api/groups/keycloak",
            success: function(data)
            {
                console.log('SUCCESS')
                // $("#modalImport").modal('hide');
                // users_table.ajax.reload();
                // groups_table.ajax.reload();
            },
            error: function(data)
            {
                alert('Something went wrong on our side...')
            }
        });
    });

	//DataTable Main renderer
	var table = $('#groups').DataTable({
        "ajax": {
            "url": "/api/groups",
            "dataSrc": ""
        },
        "language": {
            "loadingRecords": '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
            "emptyTable": "<h1>You don't have any group created yet.</h1><br><h2>Create one using the +Add new button on top right of this page.</h2>"
        },           
        "rowId": "id",
        "deferRender": true,
        "columns": [
            {
            "className":      'details-control',
            "orderable":      false,
            "data":           null,
            "width": "10px",
            "defaultContent": '<button class="btn btn-xs btn-info" type="button"  data-placement="top" ><i class="fa fa-plus"></i></button>'
            },
            { "data": "id", "width": "10px" },
            { "data": "keycloak", "width": "10px" },
            { "data": "moodle", "width": "10px" },
            { "data": "nextcloud", "width": "10px" },
            { "data": "name", "width": "10px" },
            { "data": "path", "width": "10px" },
            ],
         "order": [[3, 'asc']],
         "columnDefs": [ {
            "targets": 2,
            "render": function ( data, type, full, meta ) {
              if(full.keycloak){
                    return '<i class="fa fa-check" style="color:lightgreen"></i>'
                }else{
                    return '<i class="fa fa-close" style="color:darkred"></i>'
                };
            }},
            {
            "targets": 3,
            "render": function ( data, type, full, meta ) {
                if(full.moodle){
                    return '<i class="fa fa-check" style="color:lightgreen"></i>'
                }else{
                    return '<i class="fa fa-close" style="color:darkred"></i>'
                };
            }},
            {
                "targets": 4,
                "render": function ( data, type, full, meta ) {
                    if(full.nextcloud){
                        return '<i class="fa fa-check" style="color:lightgreen"></i>'
                    }else{
                        return '<i class="fa fa-close" style="color:darkred"></i>'
                    };
                }},
            ] 
} );
})