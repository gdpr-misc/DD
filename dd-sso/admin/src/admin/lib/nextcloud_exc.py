#
#   Copyright © 2021,2022 IsardVDI S.L.
#   Copyright © 2022 Evilham <contact@evilham.com>
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class ProviderUnauthorized(Exception):
    pass


class ProviderConnError(Exception):
    pass


class ProviderSslError(Exception):
    pass


class ProviderConnTimeout(Exception):
    pass


class ProviderError(Exception):
    pass


class ProviderItemExists(Exception):
    pass


class ProviderItemNotExists(Exception):
    pass


class ProviderUserNotExists(Exception):
    pass


class ProviderGroupNotExists(Exception):
    pass


class ProviderFolderNotExists(Exception):
    pass


class ProviderOpError(Exception):
    pass
